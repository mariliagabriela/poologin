create database meubanco;
use meubanco;
CREATE TABLE `usuario` (
  `usuario` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `meubanco`.`usuario`
(`usuario`,`senha`)
VALUES
("marilia","123");

INSERT INTO `meubanco`.`usuario`
(`usuario`,`senha`)
VALUES
("gabriela","123");

CREATE TABLE `funcionarios` (
  `idFuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `dataNascimento` datetime(6) DEFAULT NULL,
  `profissao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idFuncionario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
