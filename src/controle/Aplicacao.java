package controle;

import modelo.Usuario;
import visao.JanelaPrincipal;

/**
 *
 * @author Gabriela Flores e Marília Rosa
 */

public class Aplicacao {
    public static void main(String[] args) {
        JanelaPrincipal jp = new JanelaPrincipal();
        jp.setVisible(true);
        Usuario user = new Usuario();
        UsuarioControle uc = new UsuarioControle(jp, user);
    }
}
