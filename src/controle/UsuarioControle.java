package controle;

import dao.UsuarioDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.Usuario;
import visao.JanelaInterna;
import visao.JanelaPrincipal;

/**
 *
 * @author Gabriela Flores e Marília Rosa
 */

public class UsuarioControle implements ActionListener{

    private JanelaPrincipal jp;
    private JanelaInterna JI;
    private Usuario user;
    private UsuarioDAO userDao;
    private FuncionarioControle funcionarioControle;
    
    public UsuarioControle(JanelaPrincipal l, Usuario u) {
        this.jp = l;
        this.JI = new JanelaInterna();
        this.user = u;
        this.userDao = new UsuarioDAO();
        this.funcionarioControle = new FuncionarioControle(this.JI);
        
        registraListeners();
    }
    
    public void registraListeners(){
        this.jp.getjBAutenticar().addActionListener(this);
        this.jp.getjMICadastrar().addActionListener(this);
    }

    public boolean autenticarUsuario(Usuario u){
        return userDao.autenticarUsuario(this.user);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Autenticar")){
            this.user.setUsuario(this.jp.getjTUsuario().getText());
            //char senha[] = this.login.getjPFSenha().getPassword();
            this.user.setSenha(this.jp.getjPFSenha().getText());
            
            if(autenticarUsuario(this.user)){
                this.jp.getjMenu().setEnabled(true);
                JOptionPane.showMessageDialog(this.jp, "Login realizado com sucesso! Menu habilitado para cadastro!");
            }else{
                 JOptionPane.showMessageDialog(this.jp, "Erro na Autenticação");
                
            }
        }
        
        if(e.getActionCommand().equals("Criar Cadastro")){
            this.jp.getjDPAreaDeTrabalho().add(this.JI);
            JI.setVisible(true);
         
        }
        
        
    }
    
}
