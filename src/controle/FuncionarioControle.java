package controle;

import dao.FuncionarioDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import modelo.Funcionario;
import visao.JanelaInterna;

/**
 *
 * @author Gabriela Flores e Marília Rosa
 */
public class FuncionarioControle implements ActionListener {

    private Funcionario F;
    private JanelaInterna JI;
    private FuncionarioDAO funcionarioDAO;

    public FuncionarioControle(JanelaInterna jan) {
        this.F = F;
        this.JI = jan;
        this.funcionarioDAO = new FuncionarioDAO();
        registrarListeners();
    }

    public void registrarListeners() {
        this.JI.getjBGravar().addActionListener(this);
        System.out.println(this.JI.getjBGravar().getActionCommand());
        this.JI.getjBLimpar().addActionListener(this);
    }

    public void limparTela() {
        this.JI.getjTNome().setText("");
        this.JI.getjRFem().setSelected(true);
        this.JI.getjTCpf().setText("");
        this.JI.getjTDataDeNascimento().setText("");
        this.JI.getjTEndereco().setText("");
        this.JI.getjCProfissao().setSelectedIndex(0);
    }

    public void armazenarCadastro() {
        this.F = new Funcionario();

        this.F.setNome(this.JI.getjTNome().getText());
        System.out.println(this.JI.getbGSexo().getSelection().getActionCommand());
        if (this.JI.getbGSexo().getSelection().getActionCommand().equals("F")) {
            this.F.setSexo("F");
        } else if (this.JI.getbGSexo().getSelection().getActionCommand().equals("M")) {
            this.F.setSexo("M");
        }

        this.F.setCpf(this.JI.getjTCpf().getText());
        this.F.setDataDeNascimento(this.JI.getjTDataDeNascimento().getText());
        this.F.setEndereco(this.JI.getjTEndereco().getText());

        int i = this.JI.getjCProfissao().getSelectedIndex();
        this.F.setProfissao(this.JI.getjCProfissao().getItemAt(i));
    }

    public boolean salvarFuncionarioBanco() {
        boolean salvouFuncionario = this.funcionarioDAO.persistirFuncionario(this.F);
        return salvouFuncionario;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Gravar")) {
            armazenarCadastro();
            if (salvarFuncionarioBanco()) {
                JOptionPane.showMessageDialog(this.JI, "Funcionario cadastrado com sucesso", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this.JI, "Funcionario não foi cadastrado!", "Falha", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals("Limpar")) {
            limparTela();
        }
    }

}
