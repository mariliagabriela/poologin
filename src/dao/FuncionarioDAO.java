package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Funcionario;

/**
 *
 * @author Gabriela Flores e Marília Rosa
 */
public class FuncionarioDAO {

    private Connection con = null;

    public FuncionarioDAO() {
    }

    public boolean persistirFuncionario(Funcionario F) {
        boolean resultado = false;

        con = ConexaoMySQL.getConexaoMySQL();
//INSERT INTO `funcionarios`
//(`idFuncionario`,`nome`,`cpf`,`sexo`,`dataNascimento`,`profissao`)VALUES
//(?,?,?,?,?,?);

        String sql = "INSERT INTO `funcionarios`(`nome`,`cpf`,`sexo`,`dataNascimento`,`profissao`)VALUES(?,?,?,?,?)";
        try {
            SimpleDateFormat formatoOriginal = new SimpleDateFormat("dd-mm-yyyy");
            SimpleDateFormat formatoDataBD = new SimpleDateFormat("yyyy-MM-dd");
            String dataFormatada = null;
            try {

                dataFormatada = formatoDataBD.format(formatoOriginal.parse(F.getDataDeNascimento()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            PreparedStatement prepS = con.prepareStatement(sql);
            prepS.setString(1, F.getNome());
            prepS.setString(2, F.getCpf());
            prepS.setString(3, F.getSexo());

            prepS.setString(4, dataFormatada);
            prepS.setString(5, F.getProfissao());

            int count = prepS.executeUpdate();
            if (count > 0) {
                resultado = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;
    }

}
