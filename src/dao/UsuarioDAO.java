package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Usuario;

/**
 *
 * @author Gabriela Flores e Marília Rosa
 */

public class UsuarioDAO {
    private Connection con = null;
    
    public UsuarioDAO(){
        
    }
    
    public boolean autenticarUsuario(Usuario u){
        boolean resultado = false;
        
        con = ConexaoMySQL.getConexaoMySQL();
        String sql = "SELECT * FROM USUARIO WHERE usuario=? AND senha=?";
        try {
            PreparedStatement prepS = con.prepareStatement(sql);
            prepS.setString(1, u.getUsuario());
            prepS.setString(2, u.getSenha());
            ResultSet res = prepS.executeQuery();
            
            while(res.next()){
                String usuarioAux = res.getString(1);
                String senhaAux = res.getString(2);
                
                if(usuarioAux.equals(u.getUsuario())){
                    resultado = true;
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultado;
    }
}
